
--[[

     Licensed under GNU General Public License v2
      * (c) 2019, Alphonse Mariyagnanaseelan

--]]

local awful = require("awful")
local gears = require("gears")
local signal = require("yaawl.util.signal")

local function factory(args)

    args                        = args or { }
    local step                  = args.step or 1
    local subscribe             = args.subscribe or true

    local command               = string.format("pulsemixer --get-volume --get-mute")
    local sinks                 = [[ sh -c 'pulsemixer --list-sink | grep "^Sink:.*Default$"' ]]
    local commands              = { }
    commands.increase           = string.format("pulsemixer --change-volume +%d", step)
    commands.decrease           = string.format("pulsemixer --change-volume -%d", step)
    commands.set_min            = string.format("pulsemixer --set-volume 0")
    commands.set_max            = string.format("pulsemixer --set-volume 100")
    commands.toggle             = string.format("pulsemixer --toggle-mute")
    commands.on                 = string.format("pulsemixer --unmute")
    commands.off                = string.format("pulsemixer --mute")
    commands.reset_audio        = string.format("pkill pulseaudio")
    commands                    = gears.table.crush(commands, args.commands or { })

    local subject               = require("yaawl.subject")(commands)

    function subject:_update(context)
        awful.spawn.easy_async(command,
            function(stdout, stderr, reason, exit_code) --luacheck: no unused
                local l, r, s = stdout:match("(%d+) (%d+)%c(%d)")
                context.percent = l and r and math.floor((l + r) / 2) or 0
                context.muted = s == "1"

                awful.spawn.easy_async(sinks,
                    function(sink_out, serr, sreason, scode) --luacheck: no unused
                        local name = sink_out:match("Name:%s*([^,]+)") -- `-` is a special character in Lua regex, `%` escapes it
                        context.sink_name = name
                        self:_apply(context)
                    end
                )
            end
        )
    end

    function subject:_reset_audio()
        awful.spawn.easy_async(commands.reset_audio,
            function(stdout, stderr, reason, exit_code) --luacheck: no unused
            end
        )
    end

    -- Subscribe to notification
    if subscribe then
        local subscribe_cmd = [[
            sh -c 'pactl subscribe 2>/dev/null | grep --line-buffered "sink #[0-9]*"'
        ]]
        awful.spawn.with_line_callback(subscribe_cmd, {
            stdout = function()
                subject:update()
            end
        })
        signal.connect_once {
            signal = "exit",
            on = awesome, --luacheck: ignore
            fn = function()
                awful.spawn("pkill -f 'pactl subscribe'")
            end,
        }
    end

    ---------------
    --  buttons  --
    ---------------

    subject.buttons = gears.table.join(
        awful.button({                    }, 1, function()
            subject:toggle()
        end),
        awful.button({                    }, 2, function()
            subject:_reset_audio()
        end),
        awful.button({                    }, 4, function()
            subject:decrease()
        end),
        awful.button({                    }, 5, function()
            subject:increase()
        end),
        awful.button({ "Control"          }, 4, function()
            subject:set_min()
        end),
        awful.button({ "Control"          }, 5, function()
            subject:set_max()
        end),
        awful.button({ "Control"          }, 1, function()
            subject:off()
        end),
        awful.button({ "Control"          }, 3, function()
            subject:on()
        end)
    )

    ------------
    --  init  --
    ------------

    subject:update()
    return subject

end

return factory
