
--[[

     Licensed under GNU General Public License v2
      * (c) 2019, Alphonse Mariyagnanaseelan

--]]

local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")

local json = require("yaawl.util").json
local file = require("yaawl.util.file")

local function factory(args)

    args                        = args or { }
    local APPID                 = args.APPID

    if not APPID then
        gears.debug.print_error("yaawl.weather: No APPID provided.")
        return
    end

    local url_current           = "https://api.openweathermap.org/data/2.5/weather?APPID=%s&units=%s&lang=%s&id=%s&q=%s"
    local url_forecast          = "https://api.openweathermap.org/data/2.5/forecast/daily?APPID=%s&units=%s&lang=%s&id=%s&q=%s&cnt=%s"
    local url                   = args.url or args.forecast and url_forecast or url_current

    local url_icon              = "https://openweathermap.org/img/wn/%s@2x.png"

    local data_dir              = args.data_dir or "/tmp"
    local units                 = args.units or "metric"
    local unit                  = units == "metric" and "°C" or "°F"
    local lang                  = args.lang or "en"
    local cnt                   = args.cnt or 5
    local city_id               = args.city_id or ""
    local query                 = args.query or ""
    local timeout               = args.timeout or 3600

    local preset                = args.preset or naughty.config.presets.normal
    local notification_timeout  = args.notification_timeout or 30
    local notification_title    = args.notification_timeout or "Weather"
    local _notification         = nil

    local subject               = require("yaawl.subject")()
    local _ts                   = data_dir .. "/yaawl-weather-ts"
    local _data                 = data_dir .. "/yaawl-weather-data"
    local _last                 = nil

    awful.spawn("mkdir -p " .. data_dir)

    function subject:_update(context)
        local ts = file.exists(_ts) and tonumber(file.first_line(_ts)) or 0
        if ts + timeout > os.time() then
            if not _last and file.exists(_data) then
                _last = json.decode(file.first_line(_data))
                self:_update_icon(context)
            elseif not _last and not file.exists(_data) then
                -- Should not happen, async call is probably being processed
                gears.debug.print_warning("yaawl.weather: Async call is probably being processed.")
            end
        end

        -- Write new timestamp
        file.write(_ts, os.time())

        local u = string.format(url, APPID, units, lang, city_id, query, cnt)
        awful.spawn.easy_async(string.format("curl -s '%s'", u),
            function(stdout, stderr, reason, exit_code) --luacheck: no unused
                if exit_code == 0 then
                    file.write(_data, stdout)
                    _last = json.decode(stdout)
                    self:_update_icon(context)
                end
            end
        )
    end

    function subject:is_error()
        return not _last or _last.cod ~= 200
    end

    function subject:_update_icon(context)
        local code = _last.weather[1].icon
        local icon_url = string.format(url_icon, code)
        local icon = string.format("%s/%s@2x.png", data_dir, code)
        awful.spawn.easy_async(string.format("curl -s '%s' --output '%s'", icon_url, icon),
            function(icon_sout, icon_serr, icon_reason, icon_exit) --luacheck: no unused
                _last.weather[1]["icon_file"] = icon
                context.data = _last
                self:_apply(context)
            end
        )
    end

    local cardinal_direction
    do
        local dir = {
            "N",
            "NNE",
            "NE",
            "ENE",
            "E",
            "ESE",
            "SE",
            "SSE",
            "S",
            "SSW",
            "SW",
            "WSW",
            "W",
            "WNW",
            "NW",
            "NNW",
            "N",
        }

        cardinal_direction = function(degrees)
            if not degrees then return "N/A" end
            return dir[math.floor((degrees % 360) / 22.5) + 1]
        end
    end

    subject:add_callback(function(x)
        if x._auto then return end

        naughty.destroy(_notification)
        _notification = naughty.notify {
            preset = preset,
            screen = preset.screen or awful.screen.focused(),
            title = preset.title or notification_title,
            timeout = preset.timeout or notification_timeout,
            text = table.concat {
                string.format("%s <i>(%s)</i>\n\n", x.data.weather[1].main, x.data.weather[1].description),
                string.format("Temperature: %s%s\n", x.data.main.temp, unit),
                string.format("             %s%s to %s%s\n\n", x.data.main.temp_min, unit, x.data.main.temp_max, unit),
                string.format("Humidity:    %s%%\n", x.data.main.humidity),
                string.format("Pressure:    %shPa\n", x.data.main.pressure),
                string.format("Clouds:      %s%%\n", x.data.clouds.all),
                string.format("Wind:        %sm/s (%s)\n\n", x.data.wind.speed, cardinal_direction(x.data.wind.deg)),
                string.format("Sunrise:     %s\n", os.date("%H:%M", x.data.sys.sunrise)),
                string.format("Sunset:      %s", os.date("%H:%M", x.data.sys.sunset)),
            },
        }
    end)

    ---------------
    --  buttons  --
    ---------------

    subject.buttons = gears.table.join(
        awful.button({                    }, 1, function()
            subject:show()
        end)
    )

    ------------
    --  init  --
    ------------

    subject:update()
    return subject

end

return factory
