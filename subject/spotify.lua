
--[[

     Licensed under GNU General Public License v2
      * (c) 2020, Sherman Mui

--]]

local awful = require("awful")
local gears = require("gears")

local function factory(args)

    args              = args or { }
    local base        = args.base or "mediacmd"

    local stopped     = "Spotify.*not running"
    local command     = "sp status"
    local commands    = { }
    commands.on       = "mediacmd Play"
    commands.off      = "mediacmd Pause"
    commands.toggle   = string.format("%s PlayPause", base)
    commands.decrease = string.format("%s Previous", base)
    commands.increase = string.format("%s Next", base)
    commands          = gears.table.crush(commands, args.commands or { })

    local subject     = require("yaawl.subject")(commands)

    function subject:_update(context)
        awful.spawn.easy_async(command,
            function(stdout, stderr, reason, exit_code) --luacheck: no unused
              if stdout:match("Playing") then
                context.status = "playing"
              elseif stdout:match("Paused") then
                context.status = "paused"
              elseif stdout:match(stopped) then
                  context.status = "stopped"
              end
              self:_apply(context)
            end
        )
    end

    ------------------
    --   functions  --
    ------------------

    -- function subject:on()
    --     awful.spawn.easy_async_with_shell(
    --         command,
    --         function(sout, serr, raison, excode) --luacheck: no unused
    --             if sout:match(stopped) then
    --                 awful.spawn.easy_async_with_shell("spotify", function(stdout, stderr, reason, exit_code) --luacheck: no unused
    --                     -- TODO when the status is showing up, then call self:on
    --                     -- self:on()
    --                     -- eventually it throws some other dbus errors, then ususally moves into a Stopped state, then Paused. Not sure if this is always the case.
    --                     self:update()
    --                 end)
    --             else
    --                 awful.spawn.easy_async_with_shell(commands.on, function(stdout, stderr, reason, exit_code) --luacheck: no unused
    --                     self:update()
    --                 end)
    --             end
    --         end
    --     )
    -- end

    ---------------
    --  buttons  --
    ---------------

    subject.buttons = gears.table.join(
        awful.button({                    }, 1, function()
            subject:toggle()
        end),
        awful.button({                    }, 2, function()
            subject:increase()
        end),
        awful.button({                    }, 3, function()
            subject:decrease()
        end)
    )

    ------------
    --  init  --
    ------------

    subject:update()
    return subject

end

return factory
