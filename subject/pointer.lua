--[[

     Licensed under GNU General Public License v2
      * (c) 2021 Sherman Mui

--]]

local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")

local function factory(args)

    args                        = args or { }
    local device                = args.device or "Mouse"
    local accel                 = -0.85
    local step                  = args.step or 0.025

    local last_value            = 1.0

    local command               = string.format("xprop list-props '%s'", device)
    local command_set_accel     = "xprop set-prop '%s' --type=float --format=32 'libinput Accel Speed' %f"
    local commands              = { }
    commands.set_default        = string.format(command_set_accel, device, accel)
    -- commands.set_min            = string.format("xrandr --output %s --brightness 0.1", display)
    -- commands.set_max            = string.format("xrandr --output %s --brightness 1.0", display)
    commands                    = gears.table.crush(commands, args.commands or { })

    local subject               = require("yaawl.subject")(commands)

    -- function subject:_update(context)
    --     awful.spawn.easy_async_with_shell(command,
    --         function(stdout, stderr, reason, exit_code) --luacheck: no unused
    --             last_value = tonumber(stdout)
    --             context.text = last_value * 100 .. "%"
    --             context.percent = last_value * 100
    --             context.redshift = context.redshift
    --             self:_apply(context)
    --         end
    --     )
    --     awful.spawn.easy_async("pgrep redshift",
    --         function(sout, serr, r, exit_code) --luacheck: no unused
    --             context.text = context.text
    --             context.percent = context.percent
    --             context.redshift = exit_code == 0
    --             self:_apply(context)
    --         end
    --     )
    -- end

    ------------------
    --   functions  --
    ------------------
    function subject:default()
      local sync = { count = 0, done = 2}
      self:_set_accel(sync)
      self:_set_xform(sync)
    end

    function subject:increase()
        last_value = (last_value + step <= 1.0) and last_value + step or 1.0
        local cmd = string.format("xrandr --output %s --brightness %f", display, last_value)
        os.execute(cmd)
        self:update()
    end

    function subject:decrease()
        last_value = (last_value - step > 0) and last_value - step or step
        local cmd = string.format("xrandr --output %s --brightness %f", display, last_value)
        os.execute(cmd)
        self:update()
    end

    function subject:toggle()
        awful.spawn.easy_async("pgrep redshift",
            function(stdout, stderr, reason, exit_code) --luacheck: no unused
                if exit_code == 0 then
                    awful.spawn.easy_async("kill " .. tonumber(stdout),
                        function(sout, serr, r, ecode) --luacheck: no unused
                            -- delay the update because redshift gradually undos the color shifting before exiting
                            gears.timer {
                                timeout = 10,
                                autostart = true,
                                callback = function() self:update() end,
                                single_shot = true
                            }
                        end
                    )
                else
                    awful.spawn({"redshift"})
                    self:update()
                end
            end
        )
    end

    function subject:_set_accel(sync)
        self:_set_prop(sync, "Logitech Gaming Mouse G402", "libinput Accel Speed", "--type=float --format=32 1.0")
    end

    function subject:_set_xform(sync)
        self:_set_prop(sync, "Logitech Gaming Mouse G402", "Coordinate Transformation Matrix",
                       "0.6, 0.000000, 0.000000, 0.000000, 0.600000, 0.000000, 0.000000, 0.000000, 2.000000")
    end

    function subject:_set_prop(sync, dev, prop, vals)
        awful.spawn.easy_async(
            string.format("xinput set-prop '%s' '%s' %s", dev, prop, vals),
            function(stdout, stderr, reason, exit_code) --luacheck: no unused
                if exit_code ~= 0 then
                    naughty.notify {
                        preset = naughty.config.presets.critical,
                        title = "xinput set-prop failed",
                        text = string.format("Setting '%s'\n%s:  %s\n%s, %s, %s, %d",
                                             dev, prop, vals,
                                             stderr, stdout, reason, exit_code),
                    }
                    return
                end

                sync.count = sync.count + 1
                if sync.count >= sync.done then
                    self:update()
                end
            end
        )
    end

    ---------------
    --  buttons  --
    ---------------

    subject.buttons = gears.table.join(
        awful.button({                    }, 1, function()
            subject:default()
        end),
        awful.button({                    }, 3, function()
            subject:toggle()
        end),
        awful.button({                    }, 4, function()
            subject:decrease()
        end),
        awful.button({                    }, 5, function()
            subject:increase()
        end),
        awful.button({ "Control"          }, 4, function()
            subject:set_min()
        end),
        awful.button({ "Control"          }, 5, function()
            subject:set_max()
        end),
        awful.button({ "Control"          }, 1, function()
            subject:set_min()
        end),
        awful.button({ "Control"          }, 3, function()
            subject:set_max()
        end)
    )

    ------------
    --  init  --
    ------------

    subject:update()
    return subject

end

return factory
