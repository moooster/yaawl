
--[[

     Licensed under GNU General Public License v2
      * (c) 2019, Alphonse Mariyagnanaseelan
     Copyright 2019 Sherman Mui

--]]

local awful = require("awful")
local gears = require("gears")

local function factory(args)

    args                        = args or { }
    local display               = args.display_name or "DisplayPort-2"
    local step                  = args.step or 0.05

    local last_value            = 1.0

    local command               = string.format("xrandr --verbose | awk '/Brightness/ { print $2; exit }'")
    local commands              = { }
    commands.set_min            = string.format("xrandr --output %s --brightness 0.1", display)
    commands.set_max            = string.format("xrandr --output %s --brightness 1.0", display)
    commands                    = gears.table.crush(commands, args.commands or { })

    local subject               = require("yaawl.subject")(commands)

    function subject:_update(context)
        awful.spawn.easy_async_with_shell(command,
            function(stdout, stderr, reason, exit_code) --luacheck: no unused
                last_value = tonumber(stdout)
                context.text = last_value * 100 .. "%"
                context.percent = last_value * 100
                context.redshift = context.redshift
                self:_apply(context)
            end
        )
        awful.spawn.easy_async("pgrep redshift",
            function(sout, serr, r, exit_code) --luacheck: no unused
                context.text = context.text
                context.percent = context.percent
                context.redshift = exit_code == 0
                self:_apply(context)
            end
        )
    end

    ------------------
    --   functions  --
    ------------------
    function subject:increase()
        last_value = (last_value + step <= 1.0) and last_value + step or 1.0
        local cmd = string.format("xrandr --output %s --brightness %f", display, last_value)
        os.execute(cmd)
        self:update()
    end

    function subject:decrease()
        last_value = (last_value - step > 0) and last_value - step or step
        local cmd = string.format("xrandr --output %s --brightness %f", display, last_value)
        os.execute(cmd)
        self:update()
    end

    function subject:toggle()
        awful.spawn.easy_async("pgrep redshift",
            function(stdout, stderr, reason, exit_code) --luacheck: no unused
                if exit_code == 0 then
                    awful.spawn.easy_async("kill " .. tonumber(stdout),
                        function(sout, serr, r, ecode) --luacheck: no unused
                            -- delay the update because redshift gradually undos the color shifting before exiting
                            gears.timer {
                                timeout = 10,
                                autostart = true,
                                callback = function() self:update() end,
                                single_shot = true
                            }
                        end
                    )
                else
                    awful.spawn({"redshift"})
                    self:update()
                end
            end
        )
    end

    ---------------
    --  buttons  --
    ---------------

    subject.buttons = gears.table.join(
        awful.button({                    }, 1, function()
            subject:show()
        end),
        awful.button({                    }, 3, function()
            subject:toggle()
        end),
        awful.button({                    }, 4, function()
            subject:decrease()
        end),
        awful.button({                    }, 5, function()
            subject:increase()
        end),
        awful.button({ "Control"          }, 4, function()
            subject:set_min()
        end),
        awful.button({ "Control"          }, 5, function()
            subject:set_max()
        end),
        awful.button({ "Control"          }, 1, function()
            subject:set_min()
        end),
        awful.button({ "Control"          }, 3, function()
            subject:set_max()
        end)
    )

    ------------
    --  init  --
    ------------

    subject:update()
    return subject

end

return factory
