
--[[

     Licensed under GNU General Public License v2
      * (c) 2019, Sherman Mui

--]]

local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")

local function factory(args)

    args                        = args or { }

    local rebootrequired_file   = args.rebootrequire_file or "/var/run/reboot-required"
    local reboot_packages_file  = args.rebootrequire_file or "/var/run/reboot-required.pkgs"
    local notify                = args.notify or false
    local preset                = args.preset or naughty.config.presets.normal
    local notification_timeout  = args.notification_timeout or 20
    local notification_title    = args.notification_title or "Reboot Required"
    local _notification         = nil

    local subject                = require("yaawl.subject")()
    local _last                 = false

    function subject:_update(context)
        local f = io.open(rebootrequired_file)
        context.last = _last
        if f ~= nil then
            context.needs_reboot = true
            io.close(f)
        else
            context.needs_reboot = false
        end
        _last = context.needs_reboot
        self:_apply(context)
    end

    subject:add_callback(function(x)
        if x._auto and (not notify or x.needs_reboot == x.last) then return end
        if not x.needs_reboot then return end

        local rebootrequired_packages = io.open(reboot_packages_file)
        local packages_requiringreboot = ""
        if rebootrequired_packages ~= nil then
            packages_requiringreboot = rebootrequired_packages:read("*all")
            io.close(rebootrequired_packages)
        end
        -- Unfortunately during startup this is executed before the naughty
        -- configuration is complete so the notification is created with the
        -- incorrect theme.
        naughty.destroy(_notification)
        _notification = naughty.notify {
            preset = preset,
            screen = preset.screen or awful.screen.focused(),
            title = preset.title or notification_title,
            timeout = preset.timeout or notification_timeout,
            text = packages_requiringreboot,
        }
    end)

    ---------------
    --  buttons  --
    ---------------

    subject.buttons = gears.table.join(
        awful.button({                    }, 1, function()
            subject:show()
        end)
    )

    subject:update()
    return subject

end

return factory
